import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  user;
  constructor() {
  this.user = 1;
   }

  changeUser(){
  this.user = 3;
  }

  ngOnInit(): void {
  }

}
