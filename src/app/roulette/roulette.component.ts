import { Component } from "@angular/core";

@Component({
    selector : "spin-roulette",
    templateUrl : "./roulette.component.html",
    styleUrls: ['./roulette.component.css']
}
)
export class RouletteComponent {
    name = 'SpinRoulette';
    userID = 1;
    rStatus = 'not spinning';

    calculateUserID(one, second){
        return one * second;
    }

    rouletteStatus(){
        setTimeout(this.rStatus = 'spinning in progress', 5000);
        this.rStatus = 'spinning completed';
    }
}
